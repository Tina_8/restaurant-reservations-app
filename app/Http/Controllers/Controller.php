<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
* @OA\Info(
*      version="1.0.0",
*      title="Laravel OpenApi Progetto Restaurant Reservation",
*      description="L5 Swagger OpenApi description",
*      @OA\Contact(
*          email="martina.canzoniere8@gmail.com"
*      ),
*      @OA\License(
*          name="Apache 2.0",
*          url="http://www.apache.org/licenses/LICENSE-2.0.html"
*      )
* )
*

* @OA\Server(
*      url="http://localhost:8000/api",
*      description="Restaurant Reservation API Server"
* )
*
*

* @OA\Tag(
*     name="Restaurant Reservation",
*     description="API Endpoints of Restaurant Reservation"
* ),
*

*   @OA\SecurityScheme(
     *    securityScheme="bearerAuth",
     *    in="header",
     *    name="bearerAuth",
     *    type="http",
     *    scheme="bearer",
     *    bearerFormat="JWT",
     * ),
*/

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
