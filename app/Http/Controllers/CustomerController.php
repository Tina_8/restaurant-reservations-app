<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerCrudRequest;
use Exception;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Utility\ResponseUtility;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\DataTableRequest;
use App\Models\Reservation;

/**
 * @OA\Schema(
 *     schema="Customer",
 *     title="Customer",
 *     required={"id", "name", "surname", "email"},
 *     @OA\Property(property="id", type="string", format="uuid", example="123e4567-e89b-12d3-a456-426614174000"),
 *     @OA\Property(property="name", type="string", maxLength=255, example="Mario"),
 *     @OA\Property(property="surname", type="string", maxLength=255, example="Rossi"),
 *     @OA\Property(property="email", type="string", format="email", example="mario.rossi@test.com"),
 *     @OA\Property(property="created_at", type="string", format="date-time"),
 *     @OA\Property(property="updated_at", type="string", format="date-time"),
 *     @OA\Property(property="deleted_at", type="string", format="date-time")
 * )
 * 
 * /**
 * @OA\Schema(
 *     schema="CustomerCrudRequest",
 *     title="Customer CRUD Request",
 *     required={"name", "surname", "email"},
 *     @OA\Property(property="name", type="string", maxLength=255, example="Carlo"),
 *     @OA\Property(property="surname", type="string", maxLength=255, example="Bianchi"),
 *     @OA\Property(property="email", type="string", format="email", maxLength=255, example="carlo53@test.it"),
 * )
 */

class CustomerController extends Controller
{
    
    /**
    * Display a listing of the resource.
    * @OA\Get(
    *     path="/customers",
    *     summary="Get a list of customers",
    *     description="Retrieve a list of customers with optional filtering and pagination.",
    *     tags={"Customer Controller"},
    *     @OA\Parameter(
    *         name="order_by",
    *         in="query",
    *         description="Sort the results by a specific field",
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="order_dir",
    *         in="query",
    *         description="Sort direction ('asc' or 'desc')",
    *         @OA\Schema(type="string", enum={"asc", "desc"})
    *     ),
    *     @OA\Parameter(
    *         name="filter_by",
    *         in="query",
    *         description="Filter customers by name or surname",
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="page",
    *         in="query",
    *         description="Page number for pagination",
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="page_length",
    *         in="query",
    *         description="Number of items per page",
    *         @OA\Schema(type="integer")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customers found successfully."),
    *             @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Customer"))
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customers not found, please add a customer to the list.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error",
    *     )
    * )
    */
    public function index(DataTableRequest $request)
    {
        try {
            $order_by = $request->order_by != null ? $request->order_by : "created_at";
            $order_dir = $request->order_dir != null ? $request->order_by : "asc";
            $filter_by =  $request->filter_by;
            $page = $request->page != null ? $request->page : 0;
            $page_length = $request->page_length != null ? $request->page_length : 10;
            $skip = $page * $page_length;

            $customer = Customer::with('reservations');

            if ($filter_by != null) {
                $customer = $customer->where('surname', 'like', '%' . $filter_by . '%')
                ->orWhere('name', 'like', '%' . $filter_by . '%');
        }

            $customer = $customer->orderBy($order_by, $order_dir)
                ->skip($skip)->take($page_length)->get();

            if(isset($customer)){
                return ResponseUtility::sendResponse('Customers found successfully.', $customer, 200);
            } else {
                return ResponseUtility::sendErrorMessage('Customers not found, please add a customer to the list.');
            }

        } catch (Exception $e){
            return ResponseUtility::unexpectedErrorHandler('CustomerController', 'index', $e);
        }
    }

    /**
    * Create a new customer.
    *
    * @OA\Post(
    *     path="/customers",
    *     summary="Create a new customer",
    *     description="Create a new customer with the provided information.",
    *     tags={"Customer Controller"},
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(ref="#/components/schemas/CustomerCrudRequest")
    *     ),
    *     @OA\Response(
    *         response=201,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer created successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Customer")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */
    public function store(CustomerCrudRequest $request)
    {
        try{
            DB::beginTransaction();

            $customer = Customer::create([
                'name' => $request->name,
                'surname' => $request->surname,
                'email' => $request->email,
            ]);

            DB::commit();
                
            return ResponseUtility::sendResponse('Customer created successfully.', $customer, 201);
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('CustomerController', 'store', $e);
        }
    }

    /**
    * Display the specified resource.
    *
    * @OA\Get(
    *     path="/customers/{customer_id}",
    *     summary="Get customer by ID",
    *     description="Retrieve customer information by ID.",
    *     tags={"Customer Controller"},
    *     @OA\Parameter(
    *         name="customer_id",
    *         in="path",
    *         required=true,
    *         description="ID of the customer to retrieve",
    *         @OA\Schema(
    *             type="string",
    *             format="uuid"  
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Selected customer found successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Customer")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer not found, please insert another customer id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */
    public function show(string $customer_id)
    {
        try{
            $customer = Customer::with('reservations')->where('id', '=', $customer_id)->first();
        
            if(isset($customer))
                return ResponseUtility::sendResponse('Selected customer find successfully.', $customer, 200);
            else 
                return ResponseUtility::sendErrorMessage('Customer not found, please insert another customer id.', 404);
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('CustomerController', 'show', $e);
        }    
    }

    /**
    * Update the specified resource in storage.
    *
    * @OA\Put(
    *     path="/customers/{customer_id}",
    *     summary="Update customer by ID",
    *     description="Update a customer's information by ID.",
    *     tags={"Customer Controller"},
    *     @OA\Parameter(
    *         name="customer_id",
    *         in="path",
    *         required=true,
    *         description="ID of the customer to update",
    *         @OA\Schema(
    *             type="string",
    *             format="uuid"  
    *         )
    *     ),
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(ref="#/components/schemas/CustomerCrudRequest")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer updated successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Customer")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer not found, please insert another customer id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */
    public function update(CustomerCrudRequest $request, string $customer_id)
    {
        try {
            DB::beginTransaction();
            $customer = Customer::find($customer_id);

            if(isset($customer)){
                $customer->fill($request->all());
                $customer->save();
                DB::commit();
                return ResponseUtility::sendResponse('Customer updated successfully.', $customer);
            } else {
                return ResponseUtility::sendErrorMessage('Customer not found, please insert another customer id.');
            }
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('CustomerController', 'update', $e);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @OA\Delete(
    *     path="/customers/{customer_id}",
    *     summary="Delete customer by ID",
    *     description="Delete a customer by ID and all associated reservations.",
    *     tags={"Customer Controller"},
    *     @OA\Parameter(
    *         name="customer_id",
    *         in="path",
    *         required=true,
    *         description="ID of the customer to delete",
    *         @OA\Schema(
    *             type="string",
    *             format="uuid"  
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer deleted successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Customer")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Customer not found, please insert another customer id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */ 
    public function destroy(string $customer_id)
    {
        try {
            $customer = Customer::find($customer_id);

            if(isset($customer)){
                $reservations = Reservation::where('customer_id', $customer_id)->delete();
                $customer->delete();
                return ResponseUtility::sendResponse('Customer deleted successfully.', $customer);
            } else {
                return ResponseUtility::sendErrorMessage('Customer not found, please insert another customer id.');
            }
        } catch (Exception $e){
            return ResponseUtility::unexpectedErrorHandler('CustomerController', 'destroy', $e);
        }
    }
}
