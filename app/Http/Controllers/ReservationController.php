<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Customer;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Models\RestaurantSlot;
use OpenApi\Annotations as OA;
use App\Utility\ResponseUtility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\DataTableRequest;
use App\Http\Requests\ReservationCrudRequest;
use App\Http\Controllers\RestaurantController;

/**
 * Restaurant Controller.
 *
 * @author  Martina Canzoniere <martina.canzoniere8@gmail.com>
 */

/**
 * @OA\Schema(
 *     schema="Reservation",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="customer_id", type="string", description="The UUID of the customer who made the reservation."),
 *     @OA\Property(property="customer_number", type="integer"),
 *     @OA\Property(property="restaurant_id", type="integer"),
 *     @OA\Property(property="restaurant_slot_id", type="integer"),
 *     @OA\Property(property="created_at", type="string", format="date-time"),
 *     @OA\Property(property="updated_at", type="string", format="date-time"),
 *     @OA\Property(property="deleted_at", type="string", format="date-time"),
 *     @OA\Property(property="restaurant", ref="#/components/schemas/Restaurant"),
 *     @OA\Property(property="restaurant_slot", ref="#/components/schemas/RestaurantSlot"),
 * )
 * 
 * @OA\Schema(
 *     schema="ReservationCrudRequest",
 *     required={"customer_name", "customer_number", "restaurant_id", "restaurant_slot_id"},
 *     @OA\Property(property="customer_name", type="string", maxLength=255, example="John Doe"),
 *     @OA\Property(property="customer_number", type="integer", minimum=1, example=4),
 *     @OA\Property(property="restaurant_id", type="integer", minimum=1, example=123),
 *     @OA\Property(property="restaurant_slot_id", type="integer", minimum=1, example=456)
 * )
 */

class ReservationController extends Controller
{
   /**
    * Display a listing of the restaurant.
    *
    * @OA\Get(
    *     path="/reservations",
    *     tags={"Reservation Controller"},
    *     summary="Returns a list of all reservation.",
    *     description="Method that returns the complete list of all reservation with pagination and filter",
    *     operationId="getAllReservation",
    *     @OA\Parameter(
    *         name="order_by",
    *         in="query",
    *         description="Column to order the results by",
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="order_dir",
    *         in="query",
    *         description="Order direction (default: asc)",
    *         @OA\Schema(type="string", enum={"asc", "desc"})
    *     ),
    *     @OA\Parameter(
    *         name="filter_by",
    *         in="query",
    *         description="Filter by customer name",
    *         required=false,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="page",
    *         in="query",
    *         description="Page number",
    *         required=false,
    *         @OA\Schema(default=0, type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="page_length",
    *         in="query",
    *         description="Number of items per page",
    *         required=false,
    *         @OA\Schema(default=10, type="integer")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="Reservation found successfully."),
    *              @OA\Property(property="data", type="object", ref="#/components/schemas/Reservation")
    *          )
    *     ),
    *     @OA\Response(
    *          response=404,
    *          description="Not Found",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="Reservation not found, please add a reservation to the list.")
    *          )
    *      ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error"
    *     )
    * )
    */
    public function index(DataTableRequest $request)
    {
        try{
            $order_by = $request->order_by != null ? $request->order_by : "created_at";
            $order_dir = $request->order_dir != null ? $request->order_by : "asc";
            $filter_by =  $request->filter_by;
            $page = $request->page != null ? $request->page : 0;
            $page_length = $request->page_length != null ? $request->page_length : 10;
            $skip = $page * $page_length;

            $reservation = Reservation::with('restaurant', 'slot', 'customer');

            if ($filter_by != null){
                $reservation = $reservation->whereHas('customer', function($query) use ($filter_by) {
                    $query->where('name', 'like', '%' . $filter_by . '%')
                    ->orWhere('surname', 'like', '%' . $filter_by . '%');
                });
            }

            $reservation = $reservation->orderBy($order_by, $order_dir)
                ->skip($skip)->take($page_length)->get();

            if(isset($reservation))
                return ResponseUtility::sendResponse('Reservation found successfully.', $reservation);
            else
                return ResponseUtility::sendErrorMessage('Reservation not found, please add a reservation to the list.');
        } catch (Exception $e){
            return ResponseUtility::unexpectedErrorHandler('ReservationController', 'index', $e);
        }
    }


    /**
    * Store a newly created resource in storage.
    * @OA\Post(
    *      path="/reservations",
    *      operationId="storeReservation",
    *      tags={"Reservation Controller"},
    *      summary="Create a new reservation.",
    *      description="Creates a new reservation for a restaurant.",
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(ref="#/components/schemas/ReservationCrudRequest")
    *     ),
    *      @OA\Response(
    *          response=201,
    *          description="Successful response.",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="Reservation created successfully."),
    *              @OA\Property(property="data", type="object", ref="#/components/schemas/Reservation")
    *          )
    *      ),
    *      @OA\Response(
    *          response=400,
    *          description="Bad Request",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="The restaurant is full, please insert another time slot.")
    *          )
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="Not Found",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="The restaurant does not work in that time slot.")
    *          )
    *      ),
    *      @OA\Response(
    *          response=500,
    *          description="Unexpected error",
    *      )
    * )
    */
    public function store(ReservationCrudRequest $request)
    {
        try{
            $selected_slot = $request->restaurant_slot_id;
            $restaurant_id = $request->restaurant_id;
            $customer_id = $request->customer_id;
            $customer_number = $request->customer_number;
            $count_remaining_covered = RestaurantController::getRemainingCoveredAttribute($selected_slot, $restaurant_id);

            $existing_slot = RestaurantSlot::where('id', $selected_slot)->where('restaurant_id', $restaurant_id)->first();
            $existing_customer = Customer::where('id', $customer_id)->first();

            if(!$existing_slot){
                return ResponseUtility::sendErrorMessage('The restaurant does not work in that time slot.');
            }

            if(!$existing_customer){
                return ResponseUtility::sendErrorMessage('There are no customers with this id, create the user first.');
            }

            if(isset($count_remaining_covered) && ($count_remaining_covered - $customer_number) > 0){
                DB::beginTransaction();
            
                $reservation = Reservation::create([
                    'customer_id' => $request->customer_id,
                    'customer_number' => $customer_number,
                    'restaurant_id' => $restaurant_id,
                    'restaurant_slot_id' => $selected_slot
                ]);

                DB::commit();
                return ResponseUtility::sendResponse('Reservation created successfully.', $reservation, 201);
            } else {
                return ResponseUtility::sendErrorMessage('The restaurant is full, please insert another time slot.', 400);
            }
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('ReservationController', 'store', $e);
        }
    }

    /**
    * Display the specified resource.
    * @OA\Get(
    *      path="/restaurants/{restaurant_id}/reservation",
    *      operationId="getReservationsByRestaurantId",
    *      tags={"Reservation Controller"},
    *      summary="Get reservations for a specific restaurant",
    *      description="Retrieves a list of reservations for a specific restaurant.",
    *      @OA\Parameter(
    *          name="restaurant_id",
    *          in="path",
    *          required=true,
    *          description="ID of the restaurant",
    *          @OA\Schema(type="integer", example=1)
    *      ),
    *      @OA\Response(
    *          response=200,
    *          description="Successful response",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="List of existing reservations for the selected restaurant."),
    *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Reservation"))
    *          )
    *      ),
    *      @OA\Response(
    *          response=404,
    *          description="Not Found",
    *          @OA\JsonContent(
    *              @OA\Property(property="message", type="string", example="No reservations found for this restaurant, please insert another restaurant id.")
    *          )
    *      ),
    *      @OA\Response(
    *          response=500,
    *          description="Unexpected error",
    *      )
    * )
    */
    public function showRestaurant(int $restaurant_id)
    {
        try{
            $reservations = Reservation::with('restaurant', 'slot', 'customer')->where('restaurant_id', $restaurant_id)->get();

        if(isset($reservations))
            return ResponseUtility::sendResponse('List of existing reservations for the selected restaurant.', $reservations, 200);
        else
            return ResponseUtility::sendErrorMessage('No reservations found for this restaurant, please insert another restaurant id.', 404);
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('ReservationController', 'showRestaurant', $e);
        }    
    }

    /**
    * Get a list of existing reservations for a customer.
    *
    * @OA\Get(
    *     path="/customers/{customer_id}/reservation",
    *     summary="Get reservations by customer id",
    *     description="Method that retrieves a list of existing reservations for the selected customer.",
    *     tags={"Reservation Controller"},
    *     @OA\Parameter(
    *         name="customer_id",
    *         in="path",
    *         required=true,
    *         description="Customer UUID",
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="List of existing reservations for the selected customer."),
    *             @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Reservation"))
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="No reservations found for this customer, please insert another customer id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error"
    *     )
    * )
    */
    public function showRestaurantByClient(string $customer_id)
    {
        try{
            $reservations = Reservation::with('restaurant', 'slot', 'customer')->where('customer_id', $customer_id)->get();

            if(isset($reservations))
                return ResponseUtility::sendResponse('List of existing reservations for the selected customer.', $reservations, 200);
            else 
                return ResponseUtility::sendErrorMessage('No reservations found for this customer, please insert another customer id.', 404);
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('ReservationController', 'showRestaurantByClient', $e);
        }    
    }
    /**
    * Update an existing reservation.
    *
    * @OA\Put(
    *     path="/reservations/{reservation_id}",
    *     summary="Update reservation",
    *     description="Method to update an existing reservation.",
    *     tags={"Reservation Controller"},
    *     @OA\Parameter(
    *         name="reservation_id",
    *         in="path",
    *         required=true,
    *         description="Reservation ID",
    *         @OA\Schema(
    *             type="integer",
    *             format="int64"
    *         )
    *     ),
    *     @OA\RequestBody(
    *         required=true,
    *         description="Reservation data",
    *         @OA\JsonContent(ref="#/components/schemas/ReservationCrudRequest")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Reservation updated successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Reservation")
    *         )
    *     ),
    *     @OA\Response(
    *         response=400,
    *         description="Bad Request",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="The restaurant does not work in that time slot.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Reservation not found, please insert id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error"
    *     )
    * )
    */
    public function update(ReservationCrudRequest $request, int $reservation_id)
    {
        try {
            DB::beginTransaction();
            $reservation = Reservation::find($reservation_id);

            if(!isset($reservation)){
                return ResponseUtility::sendErrorMessage('Reservation not found, please insert id.', 404);
            }

            $selected_slot = $request->restaurant_slot_id;
            $restaurant_id = $request->restaurant_id;
            $customer_number = $request->customer_number;
            $customer_id = $request->customer_id;

            $existing_slot = RestaurantSlot::where('id', $selected_slot)->where('restaurant_id', $restaurant_id)->first();
            $existing_customer = Customer::where('id', $customer_id)->first();

            if(!$existing_customer){
                return ResponseUtility::sendErrorMessage('There are no customers with this id, create the user first.');
            }

            if(!$existing_slot){
                return ResponseUtility::sendErrorMessage('The restaurant does not work in that time slot.');
            }


            $count_remaining_covered = RestaurantController::getRemainingCoveredAttribute($selected_slot, $restaurant_id);
            if(isset($count_remaining_covered) && ($count_remaining_covered - $customer_number) > 0){
                $reservation->fill($request->all());
                $reservation->save();
                DB::commit();
                return ResponseUtility::sendResponse('Reservation updated successfully.', $reservation);
            } else {
                return ResponseUtility::sendErrorMessage('The restaurant is full, please insert another time slot.');
            }
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('ReservationController', 'update', $e);
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @OA\Delete(
    *     path="/reservations/{reservation_id}",
    *     summary="Delete reservation by ID",
    *     description="Delete a reservation by its ID.",
    *     tags={"Reservation Controller"},
    *     @OA\Parameter(
    *         name="reservation_id",
    *         in="path",
    *         required=true,
    *         description="ID of the reservation to delete",
    *         @OA\Schema(
    *             type="integer",
    *             format="int64"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response.",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Reservation deleted successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Reservation")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Reservation not found, please insert another reservation id.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */
    public function destroy(int $reservation_id)
    {
        try {
            $reservation = Reservation::find($reservation_id);
            if(isset($reservation)){
                $reservation->delete();
                return ResponseUtility::sendResponse('Reservation deleted successfully.', $reservation);
            } else {
                return ResponseUtility::sendErrorMessage('Reservation not found, please insert another reservation id.');
            }
        } catch (Exception $e){
            return ResponseUtility::unexpectedErrorHandler('ReservationrController', 'destroy', $e);
        }
    }
}
