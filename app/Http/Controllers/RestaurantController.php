<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Restaurant;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Models\RestaurantSlot;
use App\Models\RestaurantType;
use OpenApi\Annotations as OA;
use App\Utility\ResponseUtility;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\DataTableRequest;
use App\Http\Requests\RestaurantCrudRequest;

/**
 * @OA\Schema(
 *     schema="Restaurant",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="name", type="string"),
 *     @OA\Property(property="address", type="string"),
 *     @OA\Property(property="covered", type="integer"),
 *     @OA\Property(property="created_at", type="string", format="date-time"),
 *     @OA\Property(property="updated_at", type="string", format="date-time"),
 *     @OA\Property(property="deleted_at", type="string", format="date-time"),
 * )
 *
 * @OA\Schema(
 *     schema="RestaurantSlot",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="restaurant_id", type="integer"),
 *     @OA\Property(property="start", type="string", format="date-time"),
 *     @OA\Property(property="end", type="string", format="date-time"),
 *     @OA\Property(property="created_at", type="string", format="date-time"),
 *     @OA\Property(property="updated_at", type="string", format="date-time"),
 *     @OA\Property(property="deleted_at", type="string", format="date-time"),
 *     @OA\Property(property="restaurant", ref="#/components/schemas/Restaurant"),
 * )
 *
 * @OA\Schema(
 *     schema="RestaurantType",
 *     @OA\Property(property="id", type="integer"),
 *     @OA\Property(property="restaurant_id", type="integer"),
 *     @OA\Property(property="restaurant_type", type="integer"),
 *     @OA\Property(property="created_at", type="string", format="date-time"),
 *     @OA\Property(property="updated_at", type="string", format="date-time"),
 *     @OA\Property(property="restaurant", ref="#/components/schemas/Restaurant"),
 *     @OA\Property(property="static_restaurant_type", ref="#/components/schemas/StaticRestaurantType"),
 * )
 *
 * @OA\Schema(
 *     schema="RestaurantCrudRequest",
 *     required={"name", "address", "covered"},
 *     @OA\Property(property="name", type="string", maxLength=255, example="Osteria Romana"),
 *     @OA\Property(property="address", type="string", maxLength=255, example="Via Genova 44"),
 *     @OA\Property(property="covered", type="integer", minimum=1, example=15),
 *     @OA\Property(property="restaurant_type_id", type="array", @OA\Items(type="integer")),
 *     @OA\Property(property="slots", type="array", @OA\Items(
 *         @OA\Property(property="start", type="string", pattern="^\d{2}:\d{2}$", example="13:00"),
 *         @OA\Property(property="end", type="string", pattern="^\d{2}:\d{2}$", example="14:00"),
 *     ))
 * )
 *
 * @OA\Schema(
 *     schema="StaticRestaurantType",
 *     title="Static Restaurant Type",
 *     required={"id", "cusine_type_description", "created_at", "updated_at"},
 *     @OA\Property(property="id", type="integer", format="int64", example=1),
 *     @OA\Property(property="cusine_type_description", type="string", example="Italiana"),
 *     @OA\Property(property="created_at", type="string", format="date-time", example="2023-09-01T13:34:36.000000Z"),
 *     @OA\Property(property="updated_at", type="string", format="date-time", example="2023-09-01T13:34:36.000000Z"),
 * )
 */
class RestaurantController extends Controller
{
    /**
    * Display a listing of the restaurant.
    * @OA\Get(
    *     path="/restaurants",
    *     tags={"Restaurant Controller"},
    *     summary="Returns a list of all restaurant.",
    *     description="Method that returns the complete list of all restaurant with pagination and filter.",
    *     operationId="getAllRestaurant",
    *     @OA\Parameter(
    *         name="order_by",
    *         in="query",
    *         description="Column to order the results by",
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="order_dir",
    *         in="query",
    *         description="Order direction (default: asc)",
    *         @OA\Schema(type="string", enum={"asc", "desc"})
    *     ),
    *     @OA\Parameter(
    *         name="filter_by",
    *         in="query",
    *         description="Filter by restaurant name",
    *         required=false,
    *         @OA\Schema(type="string")
    *     ),
    *     @OA\Parameter(
    *         name="page",
    *         in="query",
    *         description="Page number",
    *         required=false,
    *         @OA\Schema(default=0, type="integer")
    *     ),
    *     @OA\Parameter(
    *         name="page_length",
    *         in="query",
    *         description="Number of items per page",
    *         required=false,
    *         @OA\Schema(default=10, type="integer")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurants found successfully."),
    *             @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Restaurant"))
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurants not found, please add a restaurant to the list.")
    *         )
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error",
    *     )
    * )
    */

    public function index(DataTableRequest $request)
    {
        try {
            $order_by = $request->order_by != null ? $request->order_by : "created_at";
            $order_dir = $request->order_dir != null ? $request->order_by : "asc";
            $filter_by =  $request->filter_by;
            $page = $request->page != null ? $request->page : 0;
            $page_length = $request->page_length != null ? $request->page_length : 10;
            $skip = $page * $page_length;

            $restaurant = Restaurant::with('types','slots','reservations');
            if ($filter_by != null){
                $restaurant = $restaurant->where('name', 'like', '%' . $filter_by . '%');
            }

            $restaurant = $restaurant->orderBy($order_by, $order_dir)
                ->skip($skip)->take($page_length)->get();

            if(isset($restaurant)){
                return ResponseUtility::sendResponse('Restaurants found successfully.', $restaurant, 200);
            } else {
                return ResponseUtility::sendErrorMessage('Restaurants not found, please add a restaurant to the list.');
            }

        } catch (Exception $e){
            return ResponseUtility::unexpectedErrorHandler('RestaurantController', 'index', $e);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @OA\Post(
     *     path="/restaurants",
     *     summary="Create a new restaurant",
     *     description="Method to create a new restaurant.",
     *     tags={"Restaurant Controller"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/RestaurantCrudRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Successful response.",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Restaurant created successfully."),
     *             @OA\Property(property="data", type="array",
     *                 @OA\Items(ref="#/components/schemas/Restaurant")
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error",
     *     )
     * )
     */

    public function store(RestaurantCrudRequest $request)
    {
        try{
            DB::beginTransaction();

            $restaurant_types = $request->input('restaurant_type_id');
            $slots = $request->input('slots');

            $restaurant = Restaurant::create([
                'name' => $request->name,
                'address' => $request->address,
                'covered' => $request->covered,
            ]);

            if(isset($restaurant_types) && count($restaurant_types) > 0){
                $existingRestaurant = Restaurant::find($restaurant->id);
                if($existingRestaurant){
                    foreach ($restaurant_types as $restaurant_type){
                        $restaurant_type = RestaurantType::create([
                            'restaurant_id' => $existingRestaurant->id,
                            'restaurant_type' => $restaurant_type
                        ]);

                    }
                }
            }

            if(isset($slots)){
                foreach ($slots as $slot){
                    $restaurant->slots()->create($slot);
                }
            }

            DB::commit();
            return ResponseUtility::sendResponse('Restaurant created successfully.', $restaurant, 201);
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('RestaurantController', 'store', $e);
        }

    }

    public static function getRemainingCoveredAttribute(int $slot_id, int $restaurant_id)
    {
        $all_reserved_covered = Reservation::with('restaurant', 'slot')->where('restaurant_slot_id', $slot_id)->get();
        $restaurant = Restaurant::where('id', $restaurant_id)->first();

        if(isset($all_reserved_covered) && count($all_reserved_covered) > 0){
            foreach($all_reserved_covered as $reserved_covered){
                $reservedSlots = $reserved_covered->customer_number;
                $remaining_covered = $reserved_covered->restaurant->covered - $reservedSlots;
            }

            return $remaining_covered;
            
        } else {
            return $restaurant->covered;
        }
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/restaurants/{restaurant_id}",
     *     summary="Get restaurant by id",
     *     description="Method that shows the selected restaurant.",
     *     tags={"Restaurant Controller"},
     *     @OA\Parameter(
     *         name="restaurant_id",
     *         in="path",
     *         required=true,
     *         description="restaurant id",
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Successful response.",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Selected restaurant information retrieved successfully."),
     *             @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/Restaurant")),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not Found",
     *         @OA\JsonContent(
     *             @OA\Property(property="message", type="string", example="Restaurant not found, please select another id."),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Unexpected error",
     *     ),
     * )
     */
    public function show(int $restaurant_id)
    {
        try{
            $array_type = [];

            $restaurant = Restaurant::with('reservations', 'types')->where('id', $restaurant_id)->first();

            if(isset($restaurant)){
                if(isset($restaurant->reservations)){
                    foreach($restaurant->reservations as &$reservation){
                        $remaining_covered = $this->getRemainingCoveredAttribute($reservation->restaurant_slot_id, $restaurant_id);
                        $reservation->slot->remaining_covered = $remaining_covered;
                    }
                }

                if(isset($restaurant->types)){
                    foreach($restaurant->types as $single_type){
                        foreach($single_type['cusinetypes'] as $cusine_type){
                            array_push($array_type, $cusine_type['cusine_type_description']);
                        }
                    }
                }

                $array_type = array_unique($array_type);
                $restaurant->type = $array_type;
                unset($restaurant->types);
                unset($restaurant->slot);

                return ResponseUtility::sendResponse('Selected restaurant information retrieved successfully.', $restaurant);
            } else {
                return ResponseUtility::sendErrorMessage('Restaurant not found, please select another id.', 404);
            }
        } catch (Exception $e){
            return $e->getLine();
            return ResponseUtility::unexpectedErrorHandler('RestaurantController', 'show', $e);
        }
    }

    /**
    * Update the specified resource in storage.
    * 
    * @OA\Put(
    *     path="/restaurants/{restaurant_id}",
    *     summary="Update restaurant by ID",
    *     description="Update a restaurant's information by its ID.",
    *     tags={"Restaurant Controller"},
    *     @OA\Parameter(
    *         name="restaurant_id",
    *         in="path",
    *         required=true,
    *         description="ID of the restaurant to update",
    *         @OA\Schema(
    *             type="integer",
    *             format="int64"
    *         )
    *     ),
    *     @OA\RequestBody(
    *         required=true,
    *         @OA\JsonContent(ref="#/components/schemas/RestaurantCrudRequest")
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response.",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurant updated successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Restaurant")
    *         )
    *     ),
    *     @OA\Response(
    *         response=400,
    *         description="Bad request. Invalid input data.",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="There are reservations for those slots, please delete them before updating."),
    *         ),
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurant not found, please select another id."),
    *         ),
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
    */
    public function update(RestaurantCrudRequest $request, int $restaurant_id)
    {
        try {
            DB::beginTransaction();
            $restaurant = Restaurant::find($restaurant_id);

            if(isset($restaurant)){

                $restaurant->update([
                    'name' => $request->name,
                    'address' => $request->address,
                    'covered' => $request->covered,
                ]);

                $request_restaurant_types = $request->input('restaurant_type_id');
                $request_slots = $request->input('slots');


                if(isset($request_restaurant_types)){

                    $existing_types = RestaurantType::where('restaurant_id', $restaurant_id)->pluck('restaurant_type')->toArray();

                    $new_types = array_diff($request_restaurant_types, $existing_types);
                    $deleted_types = array_diff($existing_types, $request_restaurant_types);

                    if(!empty($new_types)){
                        foreach ($new_types as $new_types) {
                            $restaurant->types()->create(['restaurant_type' => $new_types]);
                        }
                    }

                    if (!empty($deleted_types)) {
                        RestaurantType::where('restaurant_id', $restaurant_id)
                            ->whereIn('restaurant_type', $deleted_types)
                            ->delete();
                    }
                }

                if(isset($request_slots)){

                    $existing_slots = RestaurantSlot::where('restaurant_id', $restaurant_id)->get();

                    $existing_slots_start = $existing_slots->pluck('start')->toArray();
                    $existing_slots_end = $existing_slots->pluck('end')->toArray();

                    $request_start = array_column($request_slots, 'start');
                    $request_end = array_column($request_slots, 'end');

                    foreach ($existing_slots as $existing_slot) {
                        if (!in_array($existing_slot->start, $request_start) || !in_array($existing_slot->end, $request_end)) {

                            return ResponseUtility::sendErrorMessage('There are reservations for those slots, please delete them before updating.', 400);

                            //Ho ritenuto più corretto non eliminare le prenotazioni senza restituire un messaggio d'errore
                            // $selected_slots_reservations = Reservation::with('slot')->where('restaurant_id', $restaurant_id)->where('restaurant_slot_id', $existing_slot->id)->get();
                            // foreach($selected_slots_reservations as $deleted_reservation){
                            //     $deleted_reservation->delete();
                            // }
                            // $existing_slot->delete();
                        }
                    }

                    foreach ($request_slots as $input_slot) {
                        if (!in_array($input_slot['start'], $existing_slots_start) || !in_array($input_slot['end'], $existing_slots_end)) {
                            RestaurantSlot::create([
                                'restaurant_id' => $restaurant_id,
                                'start' => $input_slot['start'],
                                'end' => $input_slot['end'],
                            ]);
                        }
                    }
                }

                DB::commit();
                return ResponseUtility::sendResponse('Restaurant updated successfully', $restaurant);
            } else {
                return ResponseUtility::sendErrorMessage('Restaurant not found, please select another id.', 404);
            }
        } catch (Exception $e){
            DB::rollBack();
            return ResponseUtility::unexpectedErrorHandler('RestaurantController', 'update', $e);
        }
    }

    /**
    * Remove the specified resource from storage.
    * @OA\Delete(
    *     path="/restaurants/{restaurant_id}",
    *     summary="Delete restaurant by ID",
    *     description="Delete a restaurant by ID, including associated reservations, types, and slots.",
    *     tags={"Restaurant Controller"},
    *     @OA\Parameter(
    *         name="restaurant_id",
    *         in="path",
    *         required=true,
    *         description="ID of the restaurant to delete",
    *         @OA\Schema(
    *             type="integer",
    *             format="int64"
    *         )
    *     ),
    *     @OA\Response(
    *         response=200,
    *         description="Successful response",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurant deleted successfully."),
    *             @OA\Property(property="data", ref="#/components/schemas/Restaurant")
    *         )
    *     ),
    *     @OA\Response(
    *         response=404,
    *         description="Not Found",
    *         @OA\JsonContent(
    *             @OA\Property(property="message", type="string", example="Restaurant not found, please select another id."),
    *         ),
    *     ),
    *     @OA\Response(
    *         response=500,
    *         description="Unexpected error."
    *     )
    * )
     */
    public function destroy(int $id)
    {
        try {

            $restaurant = Restaurant::with('reservations', 'types', 'slots')->find($id);
            if(isset($restaurant)){

                $reservations = Reservation::where('restaurant_id', $id)->delete();

                $slots = RestaurantSlot::where('restaurant_id', $id)->delete();

                foreach($restaurant->types()->get() as $type){
                    $type->delete();
                }

                $restaurant->delete();
                return ResponseUtility::sendResponse('Restaurant deleted successfully.', $restaurant);
            } else {
                return ResponseUtility::sendErrorMessage('Restaurant not found, please select another restaurant id.', 404);
            }
        } catch (Exception $e) {
            return ResponseUtility::unexpectedErrorHandler('RestaurantController', 'destroy', $e);
        }
    }

}
