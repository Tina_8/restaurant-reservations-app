<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return match ($this->method()) {
            'POST' => $this->store(),
            'PUT', 'PATCH' => $this->update(),
        };
    }

    public function store()
    {
        return [
            'customer_id' => 'required|uuid',
            'customer_number' => 'required|integer|min:1',
            'restaurant_id' => 'required|integer|min:1',
            'restaurant_slot_id' => 'required|integer|min:1',
        ];
    }

    /**
     * Get the validation rules that apply to the put/patch request.
     *
     * @return array
     */
    public function update()
    {
        return [
            'customer_id' => 'required|uuid',
            'customer_number' => 'required|integer|min:1',
            'restaurant_id' => 'required|integer|min:1',
            'restaurant_slot_id' => 'required|integer|min:1',
        ];
    }

    /**
     * Get the validation rules that apply to the delete request.
     *
     * @return array
     */
    public function destroy()
    {
        return [
            //
        ];
    }
}
