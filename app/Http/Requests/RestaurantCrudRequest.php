<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantCrudRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules()
    {
        return match ($this->method()) {
            'POST' => $this->store(),
            'PUT', 'PATCH' => $this->update(),
        };
    }

    public function store()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'covered' => 'required|integer|min:1',
            'restaurant_type_id' => 'sometimes|array',
            'restaurant_type_id.*' => 'integer',
            'slots.*.start' => 'nullable|string|size:5',
            'slots.*.end' => 'nullable|string|size:5',
        ];
    }

    /**
     * Get the validation rules that apply to the put/patch request.
     *
     * @return array
     */
    public function update()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'covered' => 'required|integer|min:1',
            'restaurant_type_id' => 'sometimes|array',
            'restaurant_type_id.*' => 'integer',
            'slots.*.start' => 'nullable|string|size:5',
            'slots.*.end' => 'nullable|string|size:5',
        ];
    }

    /**
     * Get the validation rules that apply to the delete request.
     *
     * @return array
     */
    
}
