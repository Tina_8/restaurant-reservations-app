<?php

namespace App\Models;

use App\Traits\UUID;
use App\Models\Reservation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;
    use UUID;
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [];

    protected $fillable = ['name', 'surname', 'email'];

    public function reservations(){
        return $this->hasMany(Reservation::class, 'customer_id', 'id'); 
    }
}
