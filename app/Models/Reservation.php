<?php

namespace App\Models;

use App\Models\Customer;
use App\Models\Restaurant;
use App\Models\RestaurantSlot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reservation extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [];

    protected $fillable = ['customer_id', 'customer_number', 'restaurant_slot_id', 'restaurant_id'];

    public function restaurant(){
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

    public function slot(){
        return $this->hasOne(RestaurantSlot::class, 'id', 'restaurant_slot_id'); 
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
