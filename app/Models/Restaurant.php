<?php

namespace App\Models;

use App\Models\Reservation;
use App\Models\RestaurantSlot;
use App\Models\RestaurantType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Restaurant extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = [];

    protected $fillable = ['name', 'address', 'covered'];

    public function types(){
        return $this->hasMany(RestaurantType::class, 'restaurant_id', 'id')->with('cusinetypes'); 
    }

    public function slots(){
        return $this->hasMany(RestaurantSlot::class, 'restaurant_id', 'id');
    }

    public function reservations(){
        return $this->hasMany(Reservation::class, 'restaurant_id', 'id')->with('slot');
    }
}
