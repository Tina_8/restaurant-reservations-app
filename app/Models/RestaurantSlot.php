<?php

namespace App\Models;

use App\Models\Restaurant;
use App\Models\Reservation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RestaurantSlot extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'restaurants_slots';

    protected $fillable = ['restaurant_id', 'start', 'end'];

    public function restaurants()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id', 'id'); 
    }

    public function reservations()
    {
        return $this->belongsTo(Reservation::class, 'id', 'restaurant_slot_id'); 
    }
}
