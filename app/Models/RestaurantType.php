<?php

namespace App\Models;

use App\Models\Restaurant;
use App\Models\StaticRestaurantTypes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RestaurantType extends Model
{
    use HasFactory;

    protected $table = 'restaurants_type';

    protected $fillable = ['restaurant_type', 'restaurant_id'];


    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class, 'restaurant_id');
    }

    public function cusinetypes()
    {
        return $this->hasMany(StaticRestaurantTypes::class,'id','restaurant_type');
    }
}
