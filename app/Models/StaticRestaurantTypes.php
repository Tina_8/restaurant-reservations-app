<?php

namespace App\Models;

use App\Models\RestaurantType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StaticRestaurantTypes extends Model
{
    use HasFactory;

    protected $table = 'static_restaurants_types';

    public function cusinetype()
    {
        return $this->belongsTo(RestaurantType::class);
    }
}
