<?php

namespace App\Utility;

use Exception;
use Illuminate\Support\Facades\Log;


class ResponseUtility
{

    public static function unexpectedErrorHandler($controller, $function, Exception $ex)
    {
        Log::error([$controller, $function, $ex->getMessage()]);
        return response()->json([
            'status' => 'fail',
            'message' => $ex->getMessage(),
            'ex' => $ex,
        ], 500);
    }

    public static function sendResponse(string $message, $data = null, int $code = 200)
    {
        return response()->json([
            'status' => 'success',
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public static function sendErrorMessage(string $message, int $code = 500)
    {
        return response()->json([
            'status' => 'fail',
            'message' => $message,
        ], $code);
    }
}
