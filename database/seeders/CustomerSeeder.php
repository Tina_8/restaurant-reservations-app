<?php

namespace Database\Seeders;

use App\Models\Customer;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $faker = Faker::create();
            $customers = [
                [
                    // 'id' => $faker->uuid(),
                    'id' => '71e2c434-4aa5-11ee-be56-0242ac120002',
                    'name' => 'Mario',
                    'surname' => 'Rossi',
                    'email' => 'mario_rossi74@gmail.com',
                ],
                [
                    // 'id' => $faker->uuid(),
                    'id' => '9a5575ce-4aa5-11ee-be56-0242ac120002',
                    'name' => 'Giovanna',
                    'surname' => 'Bianchi',
                    'email' => 'bianchigiovanna@gmail.com',
                ],
            ];

            foreach ($customers as $key => $value) {
                $customers = Customer::create([
                    'id' => $value['id'],
                    'name' => $value['name'],
                    'surname' => $value['surname'],
                    'email' => $value['email'],
                ]);
            }
    }
}
