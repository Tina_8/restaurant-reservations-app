<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\CustomerSeeder;
use Database\Seeders\RestaurantSeeder;
use Database\Seeders\ReservationSeeder;
use Database\Seeders\RestaurantSlotSeeder;
use Database\Seeders\RestaurantTypeSeeder;
use Database\Seeders\StaticCusineTypeSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(StaticCusineTypeSeeder::class);
        $this->call(CustomerSeeder::class);
        $this->call(RestaurantSeeder::class);
        $this->call(RestaurantSlotSeeder::class);
        $this->call(RestaurantTypeSeeder::class);
        $this->call(ReservationSeeder::class);
    }
}
