<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Reservation;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $reservation = [
            [
                'customer_id' => '71e2c434-4aa5-11ee-be56-0242ac120002',
                'customer_number' => '6',
                'restaurant_id' => '1',
                'restaurant_slot_id' => 2,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),

            ],
            [

                'customer_id' => '9a5575ce-4aa5-11ee-be56-0242ac120002',
                'customer_number' => '2',
                'restaurant_id' => '2',
                'restaurant_slot_id' => 1,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
        ];

        foreach ($reservation as $key => $value) {
            $reservation = Reservation::create([
                'customer_id' => $value['customer_id'],
                'customer_number' => $value['customer_number'],
                'restaurant_id' => $value['restaurant_id'],
                'restaurant_slot_id' => $value['restaurant_slot_id'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],

            ]);
        }
    }
}
