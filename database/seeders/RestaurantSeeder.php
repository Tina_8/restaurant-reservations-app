<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Restaurant;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RestaurantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $restaurant = [
            [
                'name' => 'Bistrot Michelangelo',
                'address' => 'Via Roma 51',
                'covered' => 50,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),

            ],
            [

                'name' => 'Cafè del duomo',
                'address' => 'Piazza Duomo 3',
                'covered' => 30,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
        ];

        foreach ($restaurant as $key => $value) {
            $restaurant = Restaurant::create([
                'name' => $value['name'],
                'address' => $value['address'],
                'covered' => $value['covered'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],

            ]);
        }
    }
}
