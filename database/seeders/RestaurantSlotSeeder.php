<?php

namespace Database\Seeders;

use App\Models\RestaurantSlot;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RestaurantSlotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $slots = [
            [
                'restaurant_id' => 1,
                'start' => '13:00',
                'end' => '14:00',
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),

            ],
            [

                'restaurant_id' => 2,
                'start' => '20:00',
                'end' => '21:00',
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
        ];

        foreach ($slots as $key => $value) {
            $slots = RestaurantSlot::create([
                'restaurant_id' => $value['restaurant_id'],
                'start' => $value['start'],
                'end' => $value['end'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],

            ]);
        }
    }
}
