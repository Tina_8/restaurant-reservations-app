<?php

namespace Database\Seeders;

use App\Models\RestaurantType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RestaurantTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $type = [
            [
                'restaurant_id' => 1,
                'restaurant_type' => 3,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),

            ],
            [

                'restaurant_id' => 2,
                'restaurant_type' => 1,
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
        ];

        foreach ($type as $key => $value) {
            $type = RestaurantType::create([
                'restaurant_id' => $value['restaurant_id'],
                'restaurant_type' => $value['restaurant_type'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],

            ]);
        }
    }
}
