<?php

namespace Database\Seeders;

use App\Models\StaticRestaurantTypes;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StaticCusineTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $type = [
            [
                "cusine_type_description" => "Italiana",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s"),

            ],
            [

                "cusine_type_description" => "Francese",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
            [

                "cusine_type_description" => "Giapponese",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
            [

                "cusine_type_description" => "Thailandese",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
            [

                "cusine_type_description" => "Brasiliana",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
            [

                "cusine_type_description" => "Cinese",
                "created_at" => Carbon::now()->format("Y-m-d H:i:s"),
                "updated_at" => Carbon::now()->format("Y-m-d H:i:s")

            ],
        ];

        foreach ($type as $key => $value) {
            $type = StaticRestaurantTypes::create([
                'cusine_type_description' => $value['cusine_type_description'],
                'created_at' => $value['created_at'],
                'updated_at' => $value['updated_at'],

            ]);
        }
    }
}
