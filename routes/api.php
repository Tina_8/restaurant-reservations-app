<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\RestaurantController;
use App\Http\Controllers\ReservationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('restaurants', RestaurantController::class);
Route::apiResource('reservations', ReservationController::class);
Route::apiResource('customers', CustomerController::class);
Route::get('restaurants/{restaurant_id}/reservation', [ReservationController::class, 'showRestaurant']);
Route::get('customers/{customer_id}/reservation', [ReservationController::class, 'showRestaurantByClient']);
